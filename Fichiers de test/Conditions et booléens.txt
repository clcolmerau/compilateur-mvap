// Déclarations
rat a;
rat b;
int c;
int d;
bool e;
bool f;

// Initialisations
a = 1/2;
b = 3/2;
c = 1;
d = 0;
e = true;
f = false;

// Conditions sous la forme si..sinon..
si a <> b
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si 3/2 <> 1/2
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}


si non(c == d)
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si d < c
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si c > d
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si c >= d
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si d <= c
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si a <> b
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

si 0 <> 1
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}


si non(e et f)
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}
si e ou f
{
    afficher(1/1);
}
sinon
{
    afficher(0/1);
}

