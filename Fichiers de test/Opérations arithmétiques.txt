// Déclaration
rat a;
rat b;
rat c;

// Assignation des valeurs
a = 1/2;
b = 3/2; // Pour tester la simplification
c = 1/3; // Pour tester les opérations avec des rationnels avec ≠ dénominateurs

// Simplification
afficher(a + b);
afficher(a - b);

// Dénominateurs ≠
afficher(a + c);
afficher(a - c);

// Rationnels négatifs
afficher(-a);

b = 1/2;
a = 3/9;
c = a : b;
afficher(c);
afficher(a:b);
afficher(1/2:3/9);