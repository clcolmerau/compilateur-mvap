// Ugo CESANO         22007734
// Clement COLMERAUER 22107959

grammar Rationnel;


@parser::header
{
    import java.util.HashMap;
    import java.lang.StringBuilder;
    import java.io.File;
    import java.io.FileWriter;
    import java.io.BufferedWriter;
    import java.io.IOException;
}


@parser::members
{
    HashMap<String, Integer> registre   = new HashMap<String, Integer>();
    Integer                  adresse    = 0;
    int                      label      = 0;
    int                      sp         = 0;


    public String pgcd(String a, String b)
    {
        StringBuilder code = new StringBuilder();

        // Initialisation
        code.append(a);
        code.append(b);
        code.append("STOREG 1\nSTOREG 0\nPUSHG 1\nPUSHG 0\nSUP\nJUMPF ");
        code.append(String.valueOf(label));
        code.append("\nPUSHG 0\nPUSHG 1\nSTOREG 0\nSTOREG 1\nLABEL ");
        code.append(String.valueOf(label));
        code.append("\n");
        label++;

        // Boucle
        code.append("LABEL ");
        code.append(String.valueOf(label));
        code.append("\nPUSHG 1\nPUSHI 0\nNEQ\nJUMPF ");
        code.append(String.valueOf(label + 1));
        code.append("\nPUSHG 0\nPUSHG 0\nPUSHG 1\nDIV\nPUSHG 1\nMUL\nSUB\nPUSHG 1\nSTOREG 0\nSTOREG 1\nJUMP ");
        code.append(String.valueOf(label));
        code.append("\nLABEL ");
        code.append(String.valueOf(label + 1));
        code.append("\n");

        // Résultat
        code.append("PUSHG 0\n");
        label += 2;

        return code.toString();
    }


    public String ppcm(String a, String b)
    {
        StringBuilder code = new StringBuilder();

        // Calcul de la valeur absolue
        code.append(a);
        code.append(b);
        code.append("MUL\nDUP\nPUSHI 0\nINF\nJUMPF ");
        code.append(String.valueOf(label));
        code.append("\nPUSHI -1\nMUL\nLABEL ");
        code.append(String.valueOf(label));
        code.append("\n");
        label++;

        // Calcul du PGCD
        code.append(pgcd(a, b));

        // Résultat
        code.append("DIV\n");

        return code.toString();
    }
}


start: calcul EOF;


calcul returns [ StringBuilder code ]
@init
{
    $code = new StringBuilder();
    // Espace mémoire utilisé pour les calculs.
    $code.append("ALLOC 10\n\n");
    adresse += 10;
    sp      += 10;
}
@after
{
    File outputFile = new File("./a.mvap");
    try(BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) { writer.append($code); }
    catch(IOException e) { e.printStackTrace(); }
}
    : (declaration { $code.append($declaration.code.toString()); })*
    (instruction { $code.append($instruction.code.toString()); })*
    {
        $code.append("\nFREE ");
        $code.append(String.valueOf(sp));
        $code.append("\n\nHALT\n");
    }
;


finInstruction: ';';


instruction returns [ StringBuilder code]
    : a=expression finInstruction
    {
        $code = $a.code;
    }
    | b=assignation finInstruction
    {
        $code = $b.code;
    }
    | c=affichage finInstruction
    {
        $code = $c.code;
    }
    | d=structureConditionnelle
    {
        $code = $d.code;
    }
    | e=boucle
    {
        $code = $e.code;
    }
;


declaration returns [ StringBuilder code ]
    : TYPE IDENTIFIANT finInstruction
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;
        boolean exists = registre.containsKey("i_" + id) ^
                         registre.containsKey("r_" + id) ^
                         registre.containsKey("b_" + id);

        switch($TYPE.text)
        {
            case "int":
                if(!exists)
                {
                    registre.put("i_" + id, adresse);
                    $code.append("PUSHI 0\n");
                    adresse++;
                    sp++;
                }
                break;
            case "rat":
                if(!exists)
                {
                    registre.put("r_" + id, adresse);
                    $code.append("PUSHI 1\nPUSHI 0\n");
                    adresse += 2;
                    sp += 2;
                }
                break;
            case "bool":
                if(!exists)
                {
                    registre.put("b_" + id, adresse);
                    $code.append("PUSHI 0\n");
                    adresse++;
                    sp++;
                }
                break;
        }
    }
;


assignation returns [ StringBuilder code ]
    : IDENTIFIANT '=' 'lire()'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("i_" + id))
        {
            $code.append("READ\n");
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
        }
        else if(registre.containsKey("r_" + id))
        {
            $code.append("READ\n");
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\nREAD\n");
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\n");
        }
        else if(registre.containsKey("b_" + id))
        {
            $code.append("READ\n");
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("b_" + id)));
            $code.append("\n");
        }
    }
    | IDENTIFIANT '=' a=expression
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        $code.append($a.code.toString());
        if(registre.containsKey("i_" + id))
        {
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
            sp--;
        }
        else if(registre.containsKey("r_" + id))
        {
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\nSTOREG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\n");
            sp -= 2;
        }
        else if(registre.containsKey("b_" + id))
        {
            $code.append("STOREG ");
            $code.append(String.valueOf(registre.get("b_" + id)));
            $code.append("\n");
            sp--;
        }
    }
;


expression returns [ StringBuilder code ]
    : a=expressionArithmetiqueEntiere
    {
        $code = $a.code;
    }
    | c=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
    }
    | d=expressionArithmetiqueBooleene
    {
        $code = $d.code;
    }
    | e=arrondi
    {
        $code = $e.code;
    }
    | b=affichage
    {
        $code = $b.code;
    }
    | f=expressionArithmetiqueBooleene'?'g=expression':'h=expression
    {
        $code = $f.code;
        $code.append("JUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($g.code.toString());
        $code.append("JUMP ");
        $code.append(String.valueOf(label + 2));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        $code.append($h.code.toString());
        $code.append("LABEL ");
        $code.append(String.valueOf(label + 2));
        $code.append("\n");
        label += 3;
        sp--;
    }
;


expressionArithmetiqueEntiere returns [ StringBuilder code ]
    : '('a=expressionArithmetiqueEntiere')'
    {
        $code = $a.code;
    }
    | 'num('IDENTIFIANT')'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("r_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\n");
            sp++;
        }
    }
    | 'num('a=expressionArithmetiqueEntiere')'
    {
        $code = $a.code;
        sp++;
    }
    | 'num('a=expressionArithmetiqueEntiere'/'b=expressionArithmetiqueEntiere')'
    {
        $code = $a.code;
        sp++;
    }
    | 'denum('IDENTIFIANT')'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("r_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\n");
            sp++;
        }
    }
    | 'denum('a=expressionArithmetiqueEntiere')'
    {
        $code = new StringBuilder("PUSHI 1\n");
        sp++;
    }
    | 'denum('a=expressionArithmetiqueEntiere'/'b=expressionArithmetiqueEntiere')'
    {
        $code = $b.code;
        sp++;
    }
    | 'pgcd('a=expressionArithmetiqueEntiere','b=expressionArithmetiqueEntiere')'
    {
        $code = new StringBuilder(pgcd($a.code.toString(), $b.code.toString()));
    }
    | 'ppcm('a=expressionArithmetiqueEntiere','b=expressionArithmetiqueEntiere')'
    {
        $code = new StringBuilder(ppcm($a.code.toString(), $b.code.toString()));
    }
    | 'lire()'
    {
        $code = new StringBuilder("READ\n");
        sp++;
    }
    | IDENTIFIANT'++'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("i_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\nPUSHI 1\nADD\nSTOREG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
        }
    }
    | IDENTIFIANT'--'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("i_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\nPUSHI 1\nSUB\nSTOREG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
        }
    }
    | IDENTIFIANT
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("i_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
        }
    }
    | ENTIER
    {
        $code = new StringBuilder();
        $code.append("PUSHI ");
        $code.append(String.valueOf(Integer.parseInt($ENTIER.text)));
        $code.append("\n");
        sp++;
    }
;


expressionArithmetiqueRationnelle returns [ StringBuilder code ]
    : '('c=expressionArithmetiqueRationnelle')'
    {
        $code = $c.code;
    }
    | a=expressionArithmetiqueEntiere '/' b=expressionArithmetiqueEntiere
    {
        $code = new StringBuilder();
        $code.append($b.code.toString());
        $code.append($a.code.toString());
    }
    | '-'c=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
        $code.append("STOREG 3\nPUSHI 0\nPUSHG 3\nSUB\n");
    }
    | c=expressionArithmetiqueRationnelle'*'d=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\nPUSHG 2\nPUSHG 4\nMUL\nPUSHG 3\nPUSHG 5\nMUL\n");
        sp -= 2;
    }
    | c=expressionArithmetiqueRationnelle':'d=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\nPUSHG 3\nPUSHG 4\nMUL\nPUSHG 2\nPUSHG 5\nMUL\nSTOREG 5\nSTOREG 4\nPUSHG 5\nPUSHG 4\n");
        sp -= 2;
    }
    | c=expressionArithmetiqueRationnelle'+'d=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("STOREG 3\nSTOREG 2\nSTOREG 5\nSTOREG 4\n");
        $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nADD\n");
        sp -= 2;
    }
    | c=expressionArithmetiqueRationnelle'-'d=expressionArithmetiqueRationnelle
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("STOREG 3\nSTOREG 2\nSTOREG 5\nSTOREG 4\n");
        $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 3\nPUSHI 0\nINF\nJUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHI -1\nMUL\nADD\nJUMP ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nSUB\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp -= 2;
    }
    | IDENTIFIANT'**'a=expressionArithmetiqueEntiere
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("r_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\n");

            $code.append($a.code.toString());
            $code.append("STOREG 6\nSTOREG 3\nSTOREG 2\nPUSHG 3\nSTOREG 5\nPUSHG 2\nSTOREG 4\nPUSHI 1\nSTOREG 7\nLABEL ");
            $code.append(String.valueOf(label));
            $code.append("\nPUSHG 7\nPUSHG 6\nINF\nJUMPF ");
            $code.append(String.valueOf(label + 1));
            $code.append("\nPUSHG 3\nPUSHG 5\nMUL\nSTOREG 3\nPUSHG 2\nPUSHG 4\nMUL\nSTOREG 2\nPUSHG 7\nPUSHI 1\nADD\nSTOREG 7\nJUMP ");
            $code.append(String.valueOf(label));
            $code.append("\nLABEL ");
            $code.append(String.valueOf(label + 1));
            $code.append("\nPUSHG 2\nPUSHG 3\n");
            label += 2;
            sp++;
        }
    }
    | c=expressionArithmetiqueRationnelle'**'a=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($a.code.toString());
        $code.append("STOREG 6\nSTOREG 3\nSTOREG 2\nPUSHG 3\nSTOREG 5\nPUSHG 2\nSTOREG 4\nPUSHI 1\nSTOREG 7\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 7\nPUSHG 6\nINF\nJUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nPUSHG 3\nPUSHG 5\nMUL\nSTOREG 3\nPUSHG 2\nPUSHG 4\nMUL\nSTOREG 2\nPUSHG 7\nPUSHI 1\nADD\nSTOREG 7\nJUMP ");
        $code.append(String.valueOf(label));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nPUSHG 2\nPUSHG 3\n");
        label += 2;
        sp--;
    }
    | 'sim('c=expressionArithmetiqueRationnelle')'
    {
        $code = $c.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 2\nPUSHG 3\nSTOREG 5\nSTOREG 4\n");
        $code.append(pgcd("PUSHG 2\n","PUSHG 3\n"));
        $code.append("STOREG 6\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 6\nPUSHI 1\nNEQ\nJUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        $code.append(pgcd("PUSHG 2\n","PUSHG 3\n"));
        $code.append("STOREG 6\nPUSHG 3\nPUSHG 6\nDIV\nSTOREG 3\nPUSHG 2\nPUSHG 6\nDIV\nSTOREG 2\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nPUSHG 2\nPUSHG 3\n");
        label += 2;
    }
    | IDENTIFIANT
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("r_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\n");
        }
    }
    | 'lire()'
    {
        $code = new StringBuilder("READ\nSTOREG 3\nREAD\nSTOREG 2\nPUSHG 2\nPUSHG 3\n");
        sp += 2;
    }
;


expressionArithmetiqueBooleene returns [ StringBuilder code ]
    : '('a=expressionArithmetiqueBooleene')'
    {
        $code = $a.code;
    }
    | 'non'a=expressionArithmetiqueBooleene
    {
        $code = $a.code;
        $code.append("PUSHI 1\nINF\n");
    }
    | a=expressionArithmetiqueBooleene'et'b=expressionArithmetiqueBooleene
    {
        $code = new StringBuilder($a.code.toString());
        $code.append("JUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($a.code.toString());
        $code.append($b.code.toString());
        $code.append("MUL\nJUMP ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHI 0\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
    | a=expressionArithmetiqueBooleene'ou'b=expressionArithmetiqueBooleene
    {
        $code = new StringBuilder($a.code.toString());
        $code.append("PUSHI 1\nINF\nJUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($a.code.toString());
        $code.append($b.code.toString());
        $code.append("ADD\nJUMP ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHI 1\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
    | e=comparaisonVariable
    {
        $code = $e.code;
    }
    | c=comparaisonRationnel
    {
        $code = $c.code;
    }
    | d=comparaisonEntier
    {
        $code = $d.code;
    }
    | BOOL
    {
        $code = new StringBuilder();
        $code.append("PUSHI ");
        $code.append(Boolean.parseBoolean($BOOL.text) ? "1\n" : "0\n");
        sp++;
    }
    | IDENTIFIANT
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("b_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("b_" + id)));
            $code.append("\n");
        }
    }
;


comparaisonVariable returns [ StringBuilder code ]
    : a=IDENTIFIANT'<'b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nINF\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nINF\n");

            sp++;
        }
    }
    | a=IDENTIFIANT'>'b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nSUP\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nSUP\n");

            sp++;
        }
    }
    | a=IDENTIFIANT'=='b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nEQUAL\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nEQUAL\n");

            sp++;
        }
    }
    | a=IDENTIFIANT'<>'b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nNEQ\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nNEQ\n");

            sp++;
        }
    }
    | a=IDENTIFIANT'>='b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nSUPEQ\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nSUPEQ\n");

            sp++;
        }
    }
    | a=IDENTIFIANT'<='b=IDENTIFIANT
    {
        $code = new StringBuilder();

        if(registre.containsKey("r_" + $a.text) && registre.containsKey("r_" + $b.text))
        {
            int n = registre.get("r_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            n = registre.get("r_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nPUSHG ");
            $code.append(String.valueOf(n + 1));
            $code.append("\n");

            $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
            $code.append(ppcm("PUSHG 2\n","PUSHG 4\n"));
            $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 2\nDIV\nPUSHG 6\nPUSHG 5\nMUL\nPUSHG 4\nDIV\nINFEQ\n");

            sp++;
        }

        if(registre.containsKey("i_" + $a.text) && registre.containsKey("i_" + $b.text))
        {
            int n = registre.get("i_" + $a.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\n");

            n = registre.get("i_" + $b.text);

            $code.append("PUSHG ");
            $code.append(String.valueOf(n));
            $code.append("\nINFEQ\n");

            sp++;
        }
    }
;


comparaisonRationnel returns [ StringBuilder code ]
    : e=expressionArithmetiqueRationnelle'>'f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nSUP\n");
        sp -= 3;
    }
    | e=expressionArithmetiqueRationnelle'<'f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nINF\n");
        sp -= 3;
    }
    | e=expressionArithmetiqueRationnelle'=='f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nEQUAL\n");
        sp -= 3;
    }
    | e=expressionArithmetiqueRationnelle'<>'f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nNEQ\n");
        sp -= 3;
    }
    | e=expressionArithmetiqueRationnelle'>='f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nSUPEQ\n");
        sp -= 3;
    }
    | e=expressionArithmetiqueRationnelle'<='f=expressionArithmetiqueRationnelle
    {
        $code = $e.code;
        $code.append($f.code.toString());
        $code.append("STOREG 5\nSTOREG 4\nSTOREG 3\nSTOREG 2\n");
        $code.append(ppcm("PUSHG 3\n","PUSHG 5\n"));
        $code.append("STOREG 6\nPUSHG 6\nPUSHG 3\nMUL\nPUSHG 6\nPUSHG 5\nMUL\nINFEQ\n");
        sp -= 3;
    }
;


comparaisonEntier returns [ StringBuilder code ]
    : c=expressionArithmetiqueEntiere'>'d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("SUP\n");
        sp--;
    }
    | c=expressionArithmetiqueEntiere'<'d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("INF\n");
        sp--;
    }
    | c=expressionArithmetiqueEntiere'=='d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("EQUAL\n");
        sp--;
    }
    | c=expressionArithmetiqueEntiere'<>'d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("NEQ\n");
        sp--;
    }
    | c=expressionArithmetiqueEntiere'>='d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("SUPEQ\n");
        sp--;
    }
    | c=expressionArithmetiqueEntiere'<='d=expressionArithmetiqueEntiere
    {
        $code = $c.code;
        $code.append($d.code.toString());
        $code.append("INFEQ\n");
        sp--;
    }
;


arrondi returns [ StringBuilder code ]
    : '['c=expressionArithmetiqueRationnelle']'
    {
        $code = $c.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 3\nITOF\nPUSHG 2\nITOF\nFDIV\nPUSHG 3\nPUSHG 2\nDIV\nITOF\nFSUB\nPUSHF 0.5\nFINF\nJUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 3\nPUSHG 2\nDIV\nJUMP ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 3\nPUSHG 2\nDIV\nPUSHI 1\nADD\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
    | '[-'c=expressionArithmetiqueRationnelle'-]'
    {
        $code = $c.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 3\nPUSHG 2\nDIV\n");
        sp--;
    }
    | '[+'c=expressionArithmetiqueRationnelle'+]'
    {
        $code = $c.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 3\nITOF\nPUSHG 2\nITOF\nFDIV\nPUSHG 3\nPUSHG 2\nDIV\nITOF\nFSUB\nPUSHF 0.0\nFEQUAL\nJUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 3\nPUSHG 2\nDIV\nJUMP ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\nPUSHG 3\nPUSHG 2\nDIV\nPUSHI 1\nADD\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
    | c=expressionArithmetiqueRationnelle'%'
    {
        $code = $c.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 3\nITOF\nPUSHG 2\nITOF\nFDIV\nPUSHF 100.0\nFMUL\nPUSHG 3\nITOF\nPUSHG 2\nITOF\nFDIV\nPUSHF 100.0\nFMUL\nPUSHG 3\nITOF\nPUSHG 2\nITOF\nFDIV\nPUSHF 100.0\nFMUL\nFTOI\nITOF\nFSUB\n");
        $code.append("PUSHF 0.5\nFSUP\nJUMPF ");
        $code.append(String.valueOf(label) + "\n");
        $code.append("FTOI\nPUSHI 1\nADD\nJUMP ");
        $code.append(String.valueOf(label + 1) + "\n");
        $code.append("LABEL ");
        $code.append(String.valueOf(label) + "\n");

        $code.append("FTOI\n");
        $code.append("LABEL ");
        $code.append(String.valueOf(label + 1) + "\n");
        label += 2;
        sp--;
    }
;

//Il a été décider que les affichage n'avait pas d'effet de bord sur la pile
affichage returns [ StringBuilder code ]
    : 'afficher('IDENTIFIANT')'
    {
        $code = new StringBuilder();
        String id = $IDENTIFIANT.text;

        if(registre.containsKey("i_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("i_" + id)));
            $code.append("\n");
            $code.append("WRITE\n");
            $code.append("POP\n");
        }
        else if(registre.containsKey("r_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id) + 1));
            $code.append("\nWRITE\n");
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("r_" + id)));
            $code.append("\nWRITE\n");
            $code.append("POP\nPOP\n");
        }
        else if(registre.containsKey("b_" + id))
        {
            $code.append("PUSHG ");
            $code.append(String.valueOf(registre.get("b_" + id)));
            $code.append("\n");
            $code.append("WRITE\n");
            $code.append("POP\n");
        }
    }
    | 'afficher('a=expressionArithmetiqueRationnelle')'
    {
        $code = $a.code;
        $code.append("STOREG 3\nSTOREG 2\nPUSHG 3\nWRITE\nPUSHG 2\nWRITE\nPOP\nPOP\nPUSHG 2\nPUSHG 3\n");
    }
    | 'afficher('b=arrondi')'
    {
        $code = $b.code;
        $code.append("WRITE\n");
    }
    | 'afficher('c=expressionArithmetiqueEntiere')'
    {
        $code = $c.code;
        $code.append("WRITE\n");
    }
;


structureConditionnelle returns [ StringBuilder code ]
    : 'si'a=expressionArithmetiqueBooleene b=bloc'sinon'c=bloc
    {
        $code = $a.code;
        $code.append("JUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($b.code.toString());
        $code.append("JUMP ");
        $code.append(String.valueOf(label + 2));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        $code.append($c.code.toString());
        $code.append("LABEL ");
        $code.append(String.valueOf(label + 2));
        $code.append("\n");
        label += 3;
        sp--;
    }
    | 'si'a=expressionArithmetiqueBooleene b=bloc
    {
        $code = $a.code;
        $code.append("JUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($b.code.toString());
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
;


bloc returns [ StringBuilder code ]
    @init
    {
        $code = new StringBuilder();
    }
    : '{' (instruction { $code.append($instruction.code.toString()); })* '}'
;


boucle returns [ StringBuilder code ]
    : 'repeter'c=bloc'jusque'b=expressionArithmetiqueBooleene finInstruction
    {
        $code = new StringBuilder();
        $code.append("LABEL ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($c.code.toString());
        $code.append($b.code.toString());
        $code.append("JUMPF ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        label++;
        sp--;
    }
    | 'tant que'b=expressionArithmetiqueBooleene c=bloc
    {
        $code = new StringBuilder();
        $code.append("LABEL ");
        $code.append(String.valueOf(label));
        $code.append("\n");
        $code.append($b.code.toString());
        $code.append("JUMPF ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        $code.append($c.code.toString());
        $code.append("JUMP ");
        $code.append(String.valueOf(label));
        $code.append("\nLABEL ");
        $code.append(String.valueOf(label + 1));
        $code.append("\n");
        label += 2;
        sp--;
    }
;


TYPE : 'int' | 'rat' | 'bool';
BOOL : 'true' | 'false';
NEWLINE : '\r'? '\n' -> skip;
COMMENT : '//' ~( '\n'|'\r' )* '\r'? '\n' -> skip;
ENTIER : ('0'..'9')+;
WS : (' '|'\t')+ -> skip;
IDENTIFIANT : (('a'..'z' | 'A'..'Z') + ('0'..'9')*)+;
UNMATCH : . -> skip;

// --| Code MVàP généré |------------------------------------------
// Le code MVàP généré est enregistré dans un fichier 'a.mvap'.
// ----------------------------------------------------------------
//
// --| Problème de vidage de pile |--------------------------------
// Le code MVàP généré vide la pile avant l'instruction 'HALT'
// sauf dans le cas des conditions et boucles où le vidage de pile
// ne fonctionne pas.
// Cela vient du fait que l'assembleur MVàP supprime la partie
// inutile de la condition (i.e. le code du bloc qui ne sera jamais
// exécuté) ce que le compilateur ne peut pas prévoir.
// ----------------------------------------------------------------
